// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'

import VueResource from 'vue-resource'

Vue.config.productionTip = false

Vue.use(VueResource);
Vue.use(ElementUI);

Vue.http.interceptors.push(function(request, next){
  if (request.url[0] === '/'){
    request.url = process.env.API + request.url;
  }

  next(function(response){

  });
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
