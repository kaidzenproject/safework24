import Vue from 'vue'
import Router from 'vue-router'

import Common from '@/components/layouts/Common'
import Hello from '@/components/common/Hello'
import Empty from '@/components/layouts/Empty'
import Dashboard from '@/components/layouts/Dashboard'

import Auth from '@/components/layouts/Auth'
import Login from '@/components/auth/Login'
import Register from '@/components/auth/Register'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/hello',
      component: Empty,
      children:[
        {
          path: 'hello',
          component: Hello
        }
      ]
    },
    {
      path: '/auth',
      component: Auth,
      redirect: '/auth/login',
      children: [
          {
            path: 'login',
            component: Login
          },
          {
            path: 'register',
            component: Register
          }
      ]
    },
    {
      path: '/work',
      component: Common,
      redirect: '/work/dashboard',
      children: [
        {
          path: 'dashboard',
          component: Dashboard
        }
      ]
    }
  ]
})
